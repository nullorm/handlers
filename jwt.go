package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	jwt "github.com/dgrijalva/jwt-go"
	"gopkg.in/mgo.v2/bson"
)

// AdditionalClaims claims in addition to standard claims
type AdditionalClaims struct {
	Email string        `json:"email"`
	ID    bson.ObjectId `json:"id"`
	Name  string        `json:"name"`
	jwt.StandardClaims
}

type errorResponse struct {
	Error string `json:"error"`
}

// JwtHandler middleware
type JwtHandler struct {
	handler http.Handler
}

func (l JwtHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		w.WriteHeader(http.StatusUnauthorized)
		message, _ := json.Marshal(&errorResponse{"Authorization header missing!"})
		fmt.Fprintf(w, "%s", message)
		return
	}

	authHeaderParts := strings.Split(authHeader, " ")
	if len(authHeaderParts) != 2 || strings.ToLower(authHeaderParts[0]) != "bearer" {
		w.WriteHeader(http.StatusUnauthorized)
		message, _ := json.Marshal(&errorResponse{"Authorization header corrupted!"})
		fmt.Fprintf(w, "%s", message)
		return
	}

	tokenString := authHeaderParts[1]
	token, err := jwt.ParseWithClaims(tokenString, &AdditionalClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte("keyboard cat"), nil
	})

	if err == nil {
		if claims, ok := token.Claims.(*AdditionalClaims); ok && token.Valid {
			ctx := context.WithValue(r.Context(), "user", claims)
			l.handler.ServeHTTP(w, r.WithContext(ctx))
			return
		}
	}

	w.WriteHeader(http.StatusUnauthorized)
	message, _ := json.Marshal(&errorResponse{"Invalid token!"})
	fmt.Fprintf(w, "%s", message)
	return
}

// Jwt create a Jwt handler
func Jwt(handler http.Handler) JwtHandler {
	return JwtHandler{handler}
}
