package handlers

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

// LoggingHandler middleware to log requests
type LoggingHandler struct {
	handler http.Handler
}

func (l LoggingHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	l.handler.ServeHTTP(w, r)
	// fmt.Printf("Completed %s %s in %v\n", r.Method, r.URL.Path, time.Since(start))
	if _, err := os.Stat("./traffic.log"); err != nil {
		os.Create("./traffic.log")
	}
	file, err := os.OpenFile("./traffic.log", os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		log.Printf("Somethig went wrong in opening the log file. Sufficient permissions?")
		return
	}
	s := []string{"INFO: ", "Completed", r.Method, r.URL.Path, time.Since(start).String()}
	_, err = file.WriteString(strings.Join(s, " "))
	if err != nil {
		fmt.Println(err.Error())
	}
	err = file.Close()
	if err != nil {
		fmt.Println(err)
	}
}

// Logger create a logger
func Logger(handler http.Handler) LoggingHandler {
	return LoggingHandler{handler}
}
