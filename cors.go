package handlers

import "net/http"

// CorsHandler middleware to respond to preflight requests
type CorsHandler struct {
	handler http.Handler
}

func (l CorsHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	if r.Method == "OPTIONS" {
		w.Header().Set("Access-Control-Allow-Headers", "content-type, authorization")
		w.Header().Set("Access-Control-Allow-Methods", "POST")
		w.WriteHeader(http.StatusNoContent)
		return
	}

	// set content type to json for all responses
	w.Header().Set("Content-Type", "application/json")

	l.handler.ServeHTTP(w, r)
}

// Cors create a Cors handler
func Cors(handler http.Handler) CorsHandler {
	return CorsHandler{handler}
}
